# Avaliação Backend/Frontend ou FullStack Valecard


# Agradecimento

Desde já agradecemos seu tempo para realização deste desafio e estamos torcendo por você!

## Passos iniciais
- Você deve clonar esse projeto e criar uma branch com o seu nome.
- É importante commitar todas as suas alterações na sua branch
- Não se esqueça de subir sua branch para o repositorio remoto

## Desafio BackEnd
Suponhamos que você foi contratado para criar uma Api de Produtos contendo o famoso CRUD(Create, Read, Update, Delete). Então mão na massa, mas antes leia atentamente o descritivo abaixo.
### Definição do produto

O Produto conter ter um id, nome, descrição, id da Categoria, data e hora de criação, data de alteração e status para saber se o produto está ativo ou não.

### Regras de negócios

- Toda vez que um produto for criado/persistido deve conter a data e hora de criação.
- Toda vez que um produto for alterado/update deve conter a data e hora de alteração.
- Deve ser possível ativar e desativar um produto.

### Tecnologias

Você pode utilizar tecnologias como Java 8/11, Spring Boot, Maven, Gradle, JPA, Banco de dados de sua preferência e demais frameworks que te ajudaram na entrega desse desafio. Design patterns são bem vindos.

### Teste unitários

Você pode utilizar o Junit, Mockito, TDD, BDD e  demais frameworks que te ajudaram na entrega desse desafio. Não é obrigado fazer os testes unitários mas ganha pontos por fazer.


## Desafio FrontEnd

Suponhamos que você foi contratado para criar uma (SPA) de Produtos contendo o famoso CRUD(Create, Read, Update, Delete). Então mão na massa, mas antes leia atentamente o descritivo abaixo.

### Definição da Tela

A(s) tela(s) devem ser responsivas se possível.

### Definição do produto

O Produto conter ter um id, nome, descrição, id da Categoria, data e hora de criação, data de alteração e status para saber se o produto está ativo ou não.

### Definição da Aplicação (SPA)

- Devem conter toda uma estrutura de SPA (Single Page Applications)
- A aplicação frontend deve estar bem estruturada
- A aplicação deve conter rotas
- A aplicação deve conter uma tabela para visualização dos produtos apenas ATIVOS
- Deve ser possivel a aplicação consumir uma api de produtos

### Tecnologias

Você deve utilizar tecnologias como Angular 8/12, CSS, HTML5 e typescript.


## Desafio adicional

Esse parte é opcional mas se você quizer fechar esse desafio com chave de ouro, você deve DOCKERIZAR a aplicação que você fez.


## Que a sorte e o conhecimento esteja com você !!!!!

